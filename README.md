Based on the rust gtk template, original author information as follows:
```
➜ python3 create-project.py
Welcome to GTK Rust Template
Name: Contrast
Project Name: contrast
Application ID (see: https://developer.gnome.org/ChooseApplicationID/): org.gnome.design.Contrast
Author: Bilal Elmoussaoui
Email: bil.elmoussaoui@gmail.com
Github/Gitlab repository: https://gitlab.gnome.org/World/design/contrast/
```
## Useful links in learning gtk-rs

https://blog.samwhited.com/2019/02/gtk3-patterns-in-rust-structure/